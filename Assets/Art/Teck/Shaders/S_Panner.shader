Shader "Unlit/S_Panner"
{
    Properties
    {
        _Tex0 ("Tex0", 2D) = "white" {}
        _Tex1 ("Tex1", 2D) = "white" {}
        _Tex2 ("Tex2", 2D) = "white" {}
        _Tex3 ("Tex3", 2D) = "white" {}
        _MulRGB ("Multiplier[RGB]", Range(0, 8)) = 1
        _MulA ("Multiplier[A]", Range(0, 8)) = 1
        _Mid ("Mid", Range(0, 1)) = 0.5
        _Feather ("Feather", Range(0, 1)) = 1
        _MidA ("MidA", Range(0, 1)) = 0.5
        _FeatherA ("FeatherA", Range(0, 1)) = 1
        [HDR] _Color ("Color", Color) = (1, 1, 1, 1)
        [Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend ("BlendSource", Float) = 1
        [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend ("BlendDestination", Float) = 10
        [Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull", Float) = 2
        [Toggle] _ZWrite ("ZWrite", Float) = 0
        [Toggle] _PremulA ("PreMultiply Alpha", Float) = 1
        [Toggle] _Erosion ("VC Alpha Erosion", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        Blend [_SrcBlend] [_DstBlend]
        ZWrite [_ZWrite]
        Cull [_Cull]
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                float4 uv01 : TEXCOORD0;
                float4 uv23 : TEXCOORD1;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
            };

            sampler2D _Tex0, _Tex1, _Tex2, _Tex3;
            float4 _Tex0_ST, _Tex1_ST, _Tex2_ST, _Tex3_ST;
            float _MulRGB, _MulA, _Mid, _Feather, _MidA, _FeatherA, _PremulA, _Erosion;
            float4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv01 = float4(v.uv * _Tex0_ST.xy + _Tex0_ST.zw * _Time.y + (_Tex0_ST.zw == 0 ? 0 : sign(_Tex0_ST.zw)) * v.uv.z, v.uv * _Tex1_ST.xy + _Tex1_ST.zw * _Time.y + (_Tex1_ST.zw == 0 ? 0 : sign(_Tex1_ST.zw)) * v.uv.z);
                o.uv23 = float4(v.uv * _Tex2_ST.xy + _Tex2_ST.zw * _Time.y + (_Tex2_ST.zw == 0 ? 0 : sign(_Tex2_ST.zw)) * v.uv.z, v.uv * _Tex3_ST.xy + _Tex3_ST.zw * _Time.y + (_Tex3_ST.zw == 0 ? 0 : sign(_Tex3_ST.zw)) * v.uv.z);
                o.color = v.color;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the textures
                fixed4 tex0 = tex2D(_Tex0, i.uv01.xy);
                fixed4 tex1 = tex2D(_Tex1, i.uv01.zw);
                fixed4 tex2 = tex2D(_Tex2, i.uv23.xy);
                fixed4 tex3 = tex2D(_Tex3, i.uv23.zw);
                fixed4 col = tex0 * tex1 * tex2 * tex3;
                col.a *= _MulA * lerp(1, i.color.a, _Erosion);
                col.rgb = smoothstep(saturate(_Mid - _Feather / 2), saturate(_Mid + _Feather / 2), col.rgb);
                col.a = smoothstep(saturate(_MidA - _FeatherA / 2), saturate(_MidA + _FeatherA / 2), col.a);
                col.rgb *= _MulRGB;
                col.rgb *= i.color.rgb * _Color.rgb;
                col.rgb *= lerp(1, col.a, _PremulA);
                col.a *= _Color.a * lerp(1, i.color.a, 1 - _Erosion);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
