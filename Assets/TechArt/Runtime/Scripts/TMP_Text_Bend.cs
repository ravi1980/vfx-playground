﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[ExecuteAlways]
[RequireComponent(typeof(TMP_Text))]
[HelpURL("https://docs.google.com/document/d/14XD2kKemzcG3n94Aj2lD_prPsDh4CxXG0JtNnpcHQos/edit?usp=sharing")]
public class TMP_Text_Bend : MonoBehaviour
{
    public enum WidthModes
    {
        RectTransform,
        RenderedText
    }
    public enum AnimationModes
    {
        PerCharacter,
        PerVertex
    }
    public enum OriginModes
    {
        LocalPosition,
        TransformReference
    }
    [Range(-360, 360)]
    public float Bend;
    private float m_Bend = -1;
    private float normalizedBend => m_Bend / 180;
    [Tooltip("How final bend is calculated.\r\nRectTransform: Use the entire RectTransform width [more consistent]\r\nRenderedText: Use the calculated width of the rendered text [useful for full circle]")]
    public WidthModes WidthMode;
    [Range(0, 1)]
    public float Visibility = 1;
    private float m_Visibility = -1;
    private float m_PrevVisibility;
    public AnimationCurve AppearCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public AnimationCurve AlphaCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    [Tooltip("How visibility animation is performed.\r\nPerCharacter: Each character animates as a whole\r\nPerVertex: Each character's 4 vertices animate independently in sequence [more 'squishy']")]
    public AnimationModes AnimationMode;
    public OriginModes OriginMode;
    public Vector3 AnimateOrigin;
    public Transform AnimateOriginTransform;
    private Vector3 m_AnimateOrigin
    {
        get
        {
            switch (OriginMode)
            {
                case OriginModes.TransformReference:
                    return AnimateOriginTransform ? transform.InverseTransformPoint(AnimateOriginTransform.position) : Vector3.zero;
                default:
                    // OriginModes.LocalPosition
                    return AnimateOrigin;
            }
        }
    }
    private TMP_Text m_Text;
    private List<Vector3> originalVerts = new List<Vector3>();
    private List<Vector3> verts = new List<Vector3>();
    private List<Color> colors = new List<Color>();
    private float radius;
    private float width;
    // private Vector3 center;
    private Vector2 marginMod;

    void Reset()
    {
        OriginMode = OriginModes.LocalPosition;
        AnimateOrigin = Vector3.down * m_Text.rectTransform.rect.width / 4;
        OnEnable();
        // StartCoroutine(Init(true));
    }

    void OnEnable()
    {
        m_Text = GetComponent<TMP_Text>();
        m_Text.ForceMeshUpdate();
        m_Text.mesh.GetVertices(originalVerts);
        verts = originalVerts;
        m_Text.mesh.GetColors(colors);
        // text.RegisterDirtyVerticesCallback(OnDirtyVertices);
        UpdateMeshInternal();
        // StartCoroutine(Init());
    }

    IEnumerator Init(bool reset = false)
    {
        yield return null;
        m_Text = GetComponent<TMP_Text>();
        m_Text.ForceMeshUpdate();
        m_Text.mesh.GetVertices(originalVerts);
        verts = originalVerts;
        m_Text.mesh.GetColors(colors);
        if (reset)
        {
            OriginMode = OriginModes.LocalPosition;
            AnimateOrigin = Vector3.down * m_Text.rectTransform.rect.width / 4;
        }
        // text.RegisterDirtyVerticesCallback(OnDirtyVertices);
        m_Bend = Bend;
        m_Visibility = m_PrevVisibility = Visibility;
        UpdateMeshInternal();
    }

    public void SetText(string _text)
    {
        m_Text.SetText(_text);
        UpdateMesh();
    }

    public void SetText(string _text, float _bend)
    {
        m_Text.SetText(_text);
        Bend = m_Bend = _bend;
        UpdateMesh();
    }

    public void AnimateAppear(float duration)
    {
        StartCoroutine(AnimateAppearCR(duration));
    }

    private IEnumerator AnimateAppearCR(float duration)
    {
        Visibility = 0;
        while (Visibility < 1)
        {
            Visibility += Time.deltaTime / duration;
            yield return null;
        }
        Visibility = 1;
    }

    public void UpdateMesh()
    {
        m_Text.ForceMeshUpdate();
        m_Text.mesh.GetVertices(originalVerts);
        originalVerts = verts;
        m_Text.mesh.GetColors(colors);
        UpdateMeshInternal();
    }

    void UpdateMeshInternal()
    {
        if (Mathf.Abs(normalizedBend) < 1e-06f)
        {
            m_Text.mesh.GetVertices(originalVerts);
            return;
        }
        radius = Mathf.PI * 0.1f / normalizedBend;
        switch (WidthMode)
        {
            case WidthModes.RectTransform:
                width = m_Text.rectTransform.rect.width - (m_Text.margin.x + m_Text.margin.z);
                break;
            case WidthModes.RenderedText:
                width = m_Text.renderedWidth;
                break;
        }
        // center = Vector3.up * width * -radius / 2;
        // var verts = new Vector3[originalVerts.Length];
        if (verts.Count != originalVerts.Count)
        {
            verts = originalVerts;
        }
        marginMod.x = m_Text.margin.z - m_Text.margin.x / 2;
        marginMod.y = m_Text.margin.y - m_Text.margin.w / 2;
        float x, y, v;
        int quadIndex;
        Color c;
        for (int i = 0; i < verts.Count; ++i)
        {
            x = (originalVerts[i].x + marginMod.x) / width;
            y = (originalVerts[i].y + marginMod.y) / width;
            y += radius;
            verts[i] = Vector3.up * (Mathf.Cos(x * Mathf.PI * normalizedBend) * width * y - radius * width - marginMod.y) + Vector3.right * (Mathf.Sin(x * Mathf.PI * normalizedBend) * width * y - marginMod.x);
            quadIndex = i / 4;
            v = AnimationMode == AnimationModes.PerCharacter ? quadIndex / Mathf.Min(m_Text.text.Length, (verts.Count - 1) / 4f) : Mathf.Min(m_Text.text.Length * 4, i * 1f / (verts.Count - 1));
            verts[i] = Vector3.LerpUnclamped(m_AnimateOrigin, verts[i], AppearCurve.Evaluate(Mathf.Clamp01(2 * m_Visibility - 1 + (1 - v))));
            c = colors[i];
            c.a = AppearCurve.Evaluate(Mathf.Clamp01(AlphaCurve.Evaluate(Mathf.Clamp01(2 * m_Visibility - 1 + (1 - v)))));
            colors[i] = c;
            // verts[i].y += normalizedBend * mesh.bounds.size.x / (2 * Mathf.PI);
            // verts[i] = originalVerts[i] + Vector3.up * normalizedBend * x * x - Vector3.right * normalizedBend * x * y * y * y;
        }
        if (m_Visibility < 1 || m_Visibility != m_PrevVisibility)
        {
            m_Text.mesh.SetColors(colors);
        }
        m_Text.mesh.SetVertices(verts);
        m_Text.mesh.RecalculateBounds();
        if (m_Text is TextMeshProUGUI)
        {
            m_Text.UpdateGeometry(m_Text.mesh, 0);
        }
        m_PrevVisibility = m_Visibility;
    }

    // Need this LateUpdate to support animating via AnimationClip/Timeline,
    // since there's no way to callback when fields change, and you can animate a getter/setter
    void LateUpdate()
    {
        // This check means the component will not update by just changing the text on the TextMesh
        // Use the provided TMPBend.SetText method instead
        if (Application.isPlaying && m_Bend == Bend && m_Visibility == Visibility)
        {
            return;
        }
        m_Bend = Bend;
        m_Visibility = Visibility;
        UpdateMesh();
    }

    void OnDirtyVertices()
    {
        m_Text.mesh.SetVertices(originalVerts);
        m_Text.mesh.SetColors(colors);
        UpdateMeshInternal();
    }

    public void OnDirty()
    {
        // if (m_Text == null || originalVerts.Count == 0)
        // {
        //     OnEnable();
        //     return;
        // }
        m_Text.ForceMeshUpdate();
        m_Text.mesh.SetVertices(originalVerts);
        m_Text.mesh.SetColors(colors);
        UpdateMeshInternal();
    }

    void OnDisable()
    {
        // text.UnregisterDirtyVerticesCallback(OnDirtyVertices);
        m_Text?.ForceMeshUpdate();
    }
}
