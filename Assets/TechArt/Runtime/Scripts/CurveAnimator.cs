﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class CurveAnimator : MonoBehaviour
{
    [Range(0, 1), SerializeField]
    private float t;
    private float _t;
    public int NumPoints = 10;
    public Vector3 pStart;
    public Transform tStart;
    public Vector3 pMid;
    public Transform tMid;
    public Vector3 pEnd;
    public Transform tEnd;
    public Vector3 Start => tStart ? tStart.position : pStart;
    public Vector3 Mid
    {
        get
        {
            return tMid ? tMid.position : (Start + End) / 2 + pMid * Vector3.Distance(Start, End);
        }
        set
        {
            pMid = value;
        }
    }
    public Vector3 End => tEnd ? tEnd.position : pEnd;
    public AnimationCurve Curve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve ScaleCurve = AnimationCurve.Linear(0, 1, 1, 1);
    public AnimationCurve RotationCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public Vector3 RotationAxis = Vector3.forward;
    public AnimationCurve AlphaCurve = AnimationCurve.Linear(0, 1, 1, 1);
    private Color Color;
    [System.NonSerialized]
    public float PreviewScale = 0.1f;
    public Image Image;

    public void Animate(float duration, Action onComplete = null)
    {
        Animate(duration, 0, onComplete);
    }

    public void Animate(float duration, float delay, Action onStart = null, Action onComplete = null)
    {
        StartCoroutine(AnimateCR(duration, delay, onStart, onComplete));
    }

    private IEnumerator AnimateCR(float duration, float delay, Action onStart, Action onComplete)
    {
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            yield return null;
        }
        onStart?.Invoke();
        t = 0;
        while (t < 1)
        {
            t += Time.deltaTime / duration;
            // UpdateTransform();
            yield return null;
        }
        t = 1;
        // UpdateTransform();
        onComplete?.Invoke();
    }

    private void Update()
    {
        // We're using ExecuteAlways and checking for changes in t in order to support driving this via AnimationClips (animations don't provide an OnChanged callback)
        if (_t == t)
        {
            return;
        }
        _t = t;
        UpdateTransform();
    }

    private void UpdateTransform()
    {
        transform.position = Vector3.LerpUnclamped(Vector3.Lerp(Start, Mid, _t), Vector3.Lerp(Mid, End, _t), Curve.Evaluate(_t));
        transform.localScale = Vector3.one * ScaleCurve.Evaluate(_t);
        transform.rotation = Quaternion.AngleAxis(360 * RotationCurve.Evaluate(_t), RotationAxis);
        if (Image)
        {
            Color = Image.color;
            Color.a = Mathf.LerpUnclamped(0, 1, AlphaCurve.Evaluate(_t));
            Image.color = Color;
        }
    }

    private void OnDrawGizmosSelected()
    {
        for (int i = 0; i < NumPoints; ++i)
        {
            float _t0 = i / (NumPoints - 1f);
            var p0 = Vector3.Lerp(Start, Mid, _t0);
            var p1 = Vector3.Lerp(Mid, End, _t0);
            var p = Vector3.LerpUnclamped(p0, p1, Curve.Evaluate(_t0));
            Gizmos.DrawSphere(p0, 0.1f * PreviewScale);
            Gizmos.DrawSphere(p1, 0.1f * PreviewScale);
            Gizmos.DrawSphere(p, 0.25f * PreviewScale);
        }
        Gizmos.DrawWireSphere(Start, 0.333f * PreviewScale);
        Gizmos.DrawWireSphere(Mid, 0.333f * PreviewScale);
        Gizmos.DrawWireSphere(End, 0.333f * PreviewScale);
        // Gizmos.DrawWireSphere(Vector3.LerpUnclamped(Vector3.Lerp(a, m, System.DateTime.Now.Millisecond / 1000f), Vector3.Lerp(m, b, System.DateTime.Now.Millisecond / 1000f), Curve.Evaluate(System.DateTime.Now.Millisecond / 1000f)), 0.1f);
        Gizmos.DrawWireCube(Vector3.LerpUnclamped(Vector3.Lerp(Start, Mid, System.DateTime.Now.Millisecond / 1000f), Vector3.Lerp(Mid, End, System.DateTime.Now.Millisecond / 1000f), Curve.Evaluate(System.DateTime.Now.Millisecond / 1000f)), Vector3.one * ScaleCurve.Evaluate(System.DateTime.Now.Millisecond / 1000f) * PreviewScale);
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(Vector3.LerpUnclamped(Vector3.Lerp(Start, Mid, _t), Vector3.Lerp(Mid, End, _t), Curve.Evaluate(_t)), Vector3.one * ScaleCurve.Evaluate(_t) * PreviewScale);
    }

    private void OnValidate()
    {
        UpdateTransform();
    }
}
