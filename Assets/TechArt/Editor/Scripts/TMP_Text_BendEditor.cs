﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TMP_Text_Bend))]
public class TMP_Text_BendEditor : Editor
{
    private TMP_Text_Bend textBend;
    private void OnEnable()
    {
        textBend = target as TMP_Text_Bend;
        SceneView.duringSceneGui += OnSceneGUI;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        SerializedProperty prop = serializedObject.GetIterator();
        if (prop.NextVisible(true))
        {
            do
            {
                if (textBend.OriginMode == TMP_Text_Bend.OriginModes.TransformReference
                    && prop.name == "AnimateOrigin")
                {
                    continue;
                }
                if (textBend.OriginMode == TMP_Text_Bend.OriginModes.LocalPosition
                    && prop.name == "AnimateOriginTransform")
                {
                    continue;
                }
                EditorGUILayout.PropertyField(serializedObject.FindProperty(prop.name), true);
            }
            while (prop.NextVisible(false));
        }
        if (serializedObject.ApplyModifiedProperties())
        {
            textBend.OnDirty();
        }
    }

    private void OnSceneGUI(SceneView view)
    {
        if (textBend.OriginMode == TMP_Text_Bend.OriginModes.TransformReference)
        {
            if (textBend.AnimateOriginTransform)
            {
                Handles.Label(textBend.AnimateOriginTransform.position, "Animate\r\nOrigin\r\nTransform");
            }
            return;
        }
        var t = textBend.transform;
        var p = t.TransformPoint(textBend.AnimateOrigin);
        using (var scope = new EditorGUI.ChangeCheckScope())
        {
            p = Handles.Slider2D(0, p, Vector3.zero, -t.forward, t.up, t.right, HandleUtility.GetHandleSize(p) * 0.1f, Handles.CircleHandleCap, Vector2.zero);
            Handles.Label(p, "   Animate\r\n   Origin");
            if (scope.changed)
            {
                Undo.RecordObject(textBend, "Move TextBend Animate Origin");
                textBend.AnimateOrigin = t.InverseTransformPoint(p);
                EditorUtility.SetDirty(textBend);
            }
        }
    }

    private void OnDestroy()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
    }
}
