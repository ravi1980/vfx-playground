﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CurveAnimator)), CanEditMultipleObjects]
public class CurveAnimatorEditor : Editor
{
    private CurveAnimator curve;

    private void OnEnable()
    {
        curve = target as CurveAnimator;
        SceneView.duringSceneGui += OnSceneGUI;
    }

    private void OnDisable()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
    }

    private void OnSceneGUI(SceneView view)
    {
        using (var scope = new EditorGUI.ChangeCheckScope())
        {
            if (!curve.tStart)
            {
                curve.pStart = Handles.DoPositionHandle(curve.pStart, Quaternion.identity);
            }
            if (!curve.tMid)
            {
                if (Vector3.Distance(curve.Start, curve.End) < 1e-05)
                {
                    curve.pMid = Handles.DoPositionHandle((curve.Start + curve.End) / 2, Quaternion.identity);
                }
                else
                {
                    curve.pMid = (Handles.DoPositionHandle((curve.Start + curve.End) / 2 + curve.pMid * Vector3.Distance(curve.Start, curve.End), Quaternion.identity) - (curve.Start + curve.End) / 2) / Vector3.Distance(curve.Start, curve.End);
                }
            }
            if (!curve.tEnd)
            {
                curve.pEnd = Handles.DoPositionHandle(curve.pEnd, Quaternion.identity);
            }
            // if (scope.changed)
            {
                Undo.RecordObject(curve, $"Adjust Curve[{curve.name}] handles");
                EditorUtility.SetDirty(curve);
            }
        }
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("t"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("NumPoints"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Curve"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("ScaleCurve"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("RotationCurve"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("RotationAxis"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Image"));
        if (serializedObject.FindProperty("Image").objectReferenceValue)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("AlphaCurve"));
        }
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("tStart"), new GUIContent("Start"));
        if (serializedObject.FindProperty("tStart").objectReferenceValue == null)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("pStart"), GUIContent.none);
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("tMid"), new GUIContent("Mid"));
        if (serializedObject.FindProperty("tMid").objectReferenceValue == null)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("pMid"), GUIContent.none);
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("tEnd"), new GUIContent("End"));
        if (serializedObject.FindProperty("tEnd").objectReferenceValue == null)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("pEnd"), GUIContent.none);
        }
        EditorGUILayout.EndHorizontal();
        serializedObject.ApplyModifiedProperties();
        curve.PreviewScale = EditorGUILayout.FloatField("Preview Scale", curve.PreviewScale);
    }
}
